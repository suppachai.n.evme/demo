package com.example.demo.config;

import com.example.demo.entity.DocumentContentEntity;
import com.example.demo.service.dto.DocumentContentDetailDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        final ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        PropertyMap<DocumentContentEntity, DocumentContentDetailDto> documentContentDetailDtoPropertyMap = new PropertyMap<>() {
            @Override
            protected void configure() {
                map().setNameTh(source.getAgreementDocument().getNameTh());
                map().setNameEn(source.getAgreementDocument().getNameEn());
            }
        };
        modelMapper.addMappings(documentContentDetailDtoPropertyMap);


        return modelMapper;
    }
}
