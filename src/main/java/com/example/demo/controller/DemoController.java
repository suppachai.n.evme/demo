package com.example.demo.controller;


import com.example.demo.controller.request.CreateDocumentContentRequest;
import com.example.demo.controller.response.GeneralResponse;
import com.example.demo.entity.DocumentContentEntity;
import com.example.demo.exception.DataNotFoundException;
import com.example.demo.exception.InvalidRequestException;
import com.example.demo.service.DocumentContentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.example.demo.constant.ResponseStatus.SUCCESS;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DemoController {

    private final DocumentContentService documentContentService;


    @GetMapping("/single/{codeName}")
    public GeneralResponse getTestSingleResponseTemplate(@PathVariable("codeName") String codeName) throws DataNotFoundException {
        log.info("===== Start get document content, codename: {}  {}=====", codeName,"xxxxxxxx");
        final Optional<DocumentContentEntity> documentContentEntity = documentContentService.getSingleDocumentContentSingle(codeName);
        return new GeneralResponse(SUCCESS, documentContentEntity);
    }
    @GetMapping("/all")
    public GeneralResponse getTestAllResponseTemplate() {
        final List<DocumentContentEntity> documentContentEntities = documentContentService.getAllDocumentContentAll();
        return new GeneralResponse<>(SUCCESS, documentContentEntities);
    }
    @PostMapping("/v1/document/{codeName}/versions")
    public GeneralResponse createDocumentContent(
            @PathVariable("codeName") String codeName,
            @RequestBody CreateDocumentContentRequest createDocumentContentRequest) throws InvalidRequestException {

        log.info("===== Start create document {} =====", codeName);

        final UUID id = documentContentService.createDocumentContent(codeName, createDocumentContentRequest);
        record CreateDocumentContentResponse(UUID id) {
        }

        log.info("===== End create document =====");
        return new GeneralResponse(SUCCESS, new CreateDocumentContentResponse(id));
    }
}
