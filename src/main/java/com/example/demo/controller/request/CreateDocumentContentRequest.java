package com.example.demo.controller.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateDocumentContentRequest {

    private UUID userId;
    private String contentEn;
    private String contentTh;
    private ZonedDateTime effectiveDate;
    private String remark;

}
