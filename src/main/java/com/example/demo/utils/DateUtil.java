package com.example.demo.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtil {

    public static final DateTimeFormatter DD_MMM_YY = DateTimeFormatter.ofPattern("dd MMM yy");
    public static final DateTimeFormatter DD_MMM_YY_THAI = DateTimeFormatter.ofPattern("dd MMM yy", new java.util.Locale("th", "TH"));
    public static final LocalTime MIN_TIME = LocalTime.of(0, 0, 0, 0);
    public static final LocalTime MAX_TIME = LocalTime.of(23, 59, 59, 999999000);

    private DateUtil() {
        throw new IllegalStateException("Don't initialize this class");
    }

    public static ZoneId getTimeZone() {
        return ZoneId.of("Asia/Bangkok");
    }

    public static LocalDate toLocalDate(ZonedDateTime zonedDateTime) {
        return zonedDateTime.withZoneSameInstant(getTimeZone()).toLocalDate();
    }

    public static LocalTime toLocalTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime.withZoneSameInstant(getTimeZone()).toLocalTime();
    }

    public static Date convertToDateViaInstant(LocalDate dateToConvert) {
        return Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

}
