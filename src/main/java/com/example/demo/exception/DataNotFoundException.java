package com.example.demo.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static com.example.demo.constant.ErrorCode.DATA_NOT_FOUND;

@Getter
public class DataNotFoundException extends BaseCheckedException {

    public DataNotFoundException(String message) {
        this(DATA_NOT_FOUND, message, HttpStatus.NOT_FOUND);
    }

    public DataNotFoundException(String code, String message) {
        this(code, message, HttpStatus.NOT_FOUND);
    }

    public DataNotFoundException(String code, String message, HttpStatus httpStatus) {
        super(code, message, httpStatus);
    }

}
