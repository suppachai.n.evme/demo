package com.example.demo.exception.handler;

import com.example.demo.controller.response.GeneralResponse;
import org.springframework.http.HttpStatus;

public interface ClientErrorHandler {

    <T> void handleError(HttpStatus statusCode, GeneralResponse<T> body);
}
