package com.example.demo.exception.handler;

import com.example.demo.controller.response.GeneralResponse;
import com.example.demo.exception.ClientErrorException;
import org.springframework.http.HttpStatus;

public class DefaultClientErrorHandler implements ClientErrorHandler {

    @Override
    public <T> void handleError(HttpStatus statusCode, GeneralResponse<T> responseBody) {
        throw new ClientErrorException(
                responseBody.getStatus(),
                responseBody.getMessage(),
                statusCode
        );
    }
}
