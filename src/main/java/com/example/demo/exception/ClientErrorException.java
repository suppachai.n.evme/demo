package com.example.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class ClientErrorException extends RuntimeException {
    private final String code;
    private final String message;
    private final HttpStatus httpStatus;

}
