package com.example.demo.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static com.example.demo.constant.ErrorCode.INVALID_REQUEST;

@Getter
public class InvalidRequestException extends BaseCheckedException {

    public InvalidRequestException(String message) {
        this(INVALID_REQUEST, message, HttpStatus.BAD_REQUEST);
    }

    public InvalidRequestException(String code, String message) {
        this(code, message, HttpStatus.BAD_REQUEST);
    }

    public InvalidRequestException(String code, String message, HttpStatus httpStatus) {
        super(code, message, httpStatus);
    }
}
