package com.example.demo.service.dto;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
public class DocumentContentDto {

    private UUID id;
    private String contentEn;
    private String contentTh;
    private ZonedDateTime createdDate;
    private ZonedDateTime updatedDate;
    private Integer version;
    private UUID createdBy;
    private ZonedDateTime effectiveDate;
    private String remark;
}
