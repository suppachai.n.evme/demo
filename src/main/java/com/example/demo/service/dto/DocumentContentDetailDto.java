package com.example.demo.service.dto;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
public class DocumentContentDetailDto {

    private UUID id;

    private String codeName;

    private String nameEn;

    private String nameTh;

    private String contentEn;

    private String contentTh;

    private Integer version;

    private ZonedDateTime effectiveDate;

    private UUID createdBy;

    private String remark;

    private ZonedDateTime createdDate;

    private ZonedDateTime updatedDate;
}
