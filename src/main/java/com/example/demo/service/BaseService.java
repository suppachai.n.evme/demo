package com.example.demo.service;

import com.example.demo.controller.response.GeneralResponse;
import com.example.demo.exception.ClientErrorException;
import com.example.demo.exception.handler.ClientErrorHandler;
import com.example.demo.exception.handler.DefaultClientErrorHandler;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.example.demo.constant.ResponseStatus.FAILED;


@UtilityClass
public class BaseService {

    public static <T> GeneralResponse<T> getResponse(ResponseEntity<GeneralResponse<T>> wrapResponse) {
        return getResponse(wrapResponse, new DefaultClientErrorHandler());
    }

    public static <T> GeneralResponse<T> getResponse(ResponseEntity<GeneralResponse<T>> wrapResponse, ClientErrorHandler errorHandler) {
        return extractResponse(wrapResponse, errorHandler);
    }

    public static <T> T getResponseData(ResponseEntity<GeneralResponse<T>> wrapResponse) {
        return getResponseData(wrapResponse, new DefaultClientErrorHandler());
    }

    public static <T> T getResponseData(ResponseEntity<GeneralResponse<T>> wrapResponse, ClientErrorHandler errorHandler) {
        return extractResponse(wrapResponse, errorHandler).getData();
    }

    private static <T> GeneralResponse<T> extractResponse(ResponseEntity<GeneralResponse<T>> wrapResponse, ClientErrorHandler errorHandler) {
        final GeneralResponse<T> responseBody = wrapResponse.getBody();
        if (responseBody != null) {
            if (wrapResponse.getStatusCode() != HttpStatus.OK) {
                errorHandler.handleError(wrapResponse.getStatusCode(), responseBody);
            }
            return responseBody;
        } else {
            throw new ClientErrorException(FAILED, "empty response body - unknown error", HttpStatus.BAD_GATEWAY);
        }
    }
}
