package com.example.demo.service;

import com.example.demo.controller.request.CreateDocumentContentRequest;
import com.example.demo.entity.DocumentContentEntity;
import com.example.demo.exception.DataNotFoundException;
import com.example.demo.exception.InvalidRequestException;
import com.example.demo.repository.DocumentContentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentContentService {

    private final Clock clock;
    private final ModelMapper modelMapper;
    private final DocumentContentRepository documentContentRepository;

    public List<DocumentContentEntity> getAllDocumentContentAll(){
        return documentContentRepository.findAll();
    }
    public Optional<DocumentContentEntity> getSingleDocumentContentSingle(String codeName) throws DataNotFoundException {
        Optional<DocumentContentEntity> documentContentEntities = documentContentRepository.findFirstByCodeNameOrderByVersionDesc(codeName);
        if(documentContentEntities.isEmpty()){
            throw new DataNotFoundException("Document codeName  not found");
        }

        return documentContentEntities;
    }
    @Transactional
    public UUID createDocumentContent(String codeName, CreateDocumentContentRequest createDocumentContentRequest) throws InvalidRequestException {
        log.info("Creating document content");

        final Optional<DocumentContentEntity> latestDocumentContentOptional =
                documentContentRepository.findFirstByCodeNameOrderByVersionDesc(codeName);
        int version = 1;
        ZonedDateTime latestEffectiveDate = null;
        if (latestDocumentContentOptional.isPresent()) {
            DocumentContentEntity document = latestDocumentContentOptional.get();
            version = document.getVersion() + 1;
            latestEffectiveDate = document.getEffectiveDate();
        }
//        validateEffectiveDate(createDocumentContentRequest.getEffectiveDate(), latestEffectiveDate);

        final DocumentContentEntity documentContentEntity = modelMapper.map(createDocumentContentRequest, DocumentContentEntity.class);
        documentContentEntity.setCodeName(codeName);
        documentContentEntity.setVersion(version);
        documentContentEntity.setCreatedBy(createDocumentContentRequest.getUserId());

        log.info("Document content codeName '{}' version '{}' created", codeName, version);
        return documentContentRepository.save(documentContentEntity).getId();

    }
//    private void validateEffectiveDate(ZonedDateTime effectiveDate, ZonedDateTime latestEffectiveDate) throws InvalidRequestException {
//        if (!DateUtil.toLocalDate(effectiveDate).isAfter(LocalDate.now(clock))) {
//            log.info("Input effective date '{}'", effectiveDate);
//            throw new InvalidRequestException("Effective Date should more than today");
//        }
//
//        if (latestEffectiveDate != null && !effectiveDate.isAfter(latestEffectiveDate)) {
//            log.info("latest effective date '{}' > effective date '{}'", latestEffectiveDate, effectiveDate);
//            throw new InvalidRequestException("Effective Date should more than effective date of last version");
//        }
//    }
}
