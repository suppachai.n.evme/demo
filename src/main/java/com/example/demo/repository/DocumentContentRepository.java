package com.example.demo.repository;

import com.example.demo.entity.DocumentContentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DocumentContentRepository extends JpaRepository<DocumentContentEntity, UUID> {
    Optional<DocumentContentEntity> findFirstByCodeNameOrderByVersionDesc(String codeName);
}
