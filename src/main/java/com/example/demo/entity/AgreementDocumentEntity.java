package com.example.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Table(name = "agreement_documents")
@Entity(name = "agreement_documents")
public class AgreementDocumentEntity {

    @Id
    @Column(name = "code_name")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String codeName;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "name_th")
    private String nameTh;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codeName")
    private List<DocumentContentEntity> documentContents;
}
