package com.example.demo.entity;

import com.example.demo.utils.DateUtil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Setter
@Getter
@Table(name = "document_contents")
@Entity(name = "document_contents")
public class DocumentContentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agreement_document_code_name", insertable = false, updatable = false)
    private AgreementDocumentEntity agreementDocument;

    @Column(name = "agreement_document_code_name")
    private String codeName;


    @Column(name = "content_en")
    private String contentEn;

    @Column(name = "content_th")
    private String contentTh;

    @Column
    private Integer version;

    @Column(name = "effective_date")
    private ZonedDateTime effectiveDate;

    @Column(name = "created_by")
    private UUID createdBy;

    @Column
    private String remark;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "updated_date")
    private ZonedDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = ZonedDateTime.now(DateUtil.getTimeZone());
        updatedDate = createdDate;
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = ZonedDateTime.now();
    }

}
