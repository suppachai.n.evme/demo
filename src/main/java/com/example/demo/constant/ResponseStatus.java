package com.example.demo.constant;

public class ResponseStatus {

    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";

    private ResponseStatus() {
        throw new IllegalStateException("Don't initialize this class");
    }
}
