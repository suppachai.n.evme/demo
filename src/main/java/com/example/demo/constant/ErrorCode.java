package com.example.demo.constant;

public class ErrorCode {

    public static final String DATA_NOT_FOUND = "data_not_found";
    public static final String DATA_DUPLICATE = "data_duplicate";
    public static final String INVALID_REQUEST = "invalid_request";
    public static final String MISSING_REQUIRED_FIELD = "missing_required_field";
    public static final String INVALID_EMAIL_PATTERN = "invalid_email_pattern";
    public static final String EMAIL_ALREADY_EXIST = "bad_request_email";

    private ErrorCode() {
        throw new IllegalStateException("Don't initialize this class");
    }

}
